from flask import Flask, render_template, request, url_for, redirect, session
from flask_mysqldb import MySQL
import MySQLdb.cursors
import re

app = Flask(__name__)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_PORT'] = 3309
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'db_project'
mysql = MySQL(app)


@app.route('/')
def home():
    return render_template("home.html")


@app.route('/profil')
def profil():
    return render_template("profil.html")


@app.route('/encrypt')
def encrypt():
    return render_template("encrypt.html")


@app.route('/decrypt')
def decrypt():
    return render_template("decrypt.html")


@app.route('/hasil1', methods=['POST', 'GET'])
def hasil1():
    if request.method == 'POST':
        text1 = request.form.get('text1')
        cipher = ''
        for char in text1:
            if not char.isalpha():
                continue
            char = char.upper()
            code = ord(char) + 1

            if code > ord('Z'):
                code = ord('A')

            cipher += chr(code)

    return render_template("hasil1.html", textAwal=text1, hasil1=cipher)


@app.route('/hasil2', methods=['POST', 'GET'])
def hasil2():
    if request.method == 'POST':
        text1 = request.form.get('text1')
        cipher = ''
        for char in text1:
            if not char.isalpha():
                continue
            char = char.upper()
            code = ord(char) - 1

            if code < ord('A'):
                code = ord('Z')

            cipher += chr(code)
    return render_template("hasil2.html", textAwal=text1, hasil2=cipher)

@app.route('/crud')
def index():
    cur = mysql.connection.cursor()
    cur.execute("SELECT * FROM v_dashboard")
    rv = cur.fetchall()
    cur.close()
    return render_template('index.html', data=rv)


@app.route('/crud/simpan', methods=['POST'])
def simpan():
    nama = request.form['nama']
    batch = request.form['batch']
    no_antrian = request.form['no_antrian']
    gender = request.form['gender']
    venue = request.form['venue']
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO vaksinasi (nama, batch, no_antrian, gender, venue) VALUES (%s, %s, %s, %s, %s)",
                (nama, batch, no_antrian, gender, venue))
    mysql.connection.commit()
    return redirect(url_for('index'))


@app.route('/crud/update', methods=["POST"])
def update():
    id_data = request.form['id']
    is_registrasi = request.form['is_registrasi']
    registrasi_time = request.form['registrasi_time']
    is_venue = request.form['is_venue']
    venue_time = request.form['venue_time']
    is_lolos = request.form['is_lolos']
    is_vaksinasi = request.form['is_vaksinasi']
    vaksinasi_time = request.form['vaksinasi_time']
    is_observasi = request.form['is_observasi']
    observasi_time = request.form['observasi_time']

    cur = mysql.connection.cursor()
    cur.execute("UPDATE vaksinasi SET is_registrasi=%s, registrasi_time=%s, is_venue=%s, venue_time=%s, is_lolos=%s, is_vaksinasi=%s, vaksinasi_time=%s, is_observasi=%s, observasi_time=%s  WHERE id=%s",
                (is_registrasi, registrasi_time, is_venue, venue_time, is_lolos, is_vaksinasi, vaksinasi_time, is_observasi, observasi_time, id_data))
    mysql.connection.commit()
    return redirect(url_for('index'))


@app.route('/crud/hapus/<string:id_data>', methods=["GET"])
def hapus(id_data):
    cur = mysql.connection.cursor()
    cur.execute("DELETE FROM vaksinasi WHERE id=%s", (id_data,))
    mysql.connection.commit()
    return redirect(url_for('index'))



@app.route('/login')
def login():
    return render_template("login.html")

@app.route('/auth', methods=['POST'] )
def auth():
    username = request.form['username']
    password = request.form['password']
    cipher = ''
    for char in password:
        if not char.isalpha():
            continue
        char = char.upper()
        code = ord(char) + 1

        if code > ord('Z'):
            code = ord('A')

        cipher += chr(code)
    passwordd = cipher
    cur = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
    cur.execute('SELECT * FROM users WHERE username = %s AND password = %s', (username, passwordd))
    user = cur.fetchone()

    if user:
        # session['loggedin'] = True
        # session['id'] = user['id_user']
        # session['username'] = user['username']
        return redirect(url_for('index'))
    else:
        return render_template("login.html")

@app.route('/logout')
def logout():
    # ession.pop('loggedin', None)
    # session.pop('id', None)
    # session.pop('username', None)
    return redirect(url_for('login'))

@app.route('/signup')
def signup():
    return render_template("sign_up.html")


@app.route('/register' , methods=['POST'])
def register():
    email = request.form['email']
    username = request.form['username']
    password = request.form['password1']
    cipher = ''
    for char in password:
        if not char.isalpha():
            continue
        char = char.upper()
        code = ord(char) + 1

        if code > ord('Z'):
            code = ord('A')

        cipher += chr(code)
    passwordc = cipher
    cur = mysql.connection.cursor()
    cur.execute("INSERT INTO users (id_user, username, password, email) VALUES (%s, %s, %s, %s)",
                ('', username, passwordc, email))
    mysql.connection.commit()
    return redirect(url_for('login'))



if __name__ == '__main__':
    app.run(debug=True)
